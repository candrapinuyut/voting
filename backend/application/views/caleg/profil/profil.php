<blockquote><h4>Data Profil</h4></blockquote>
<hr />
 <?php 
 
if($profil){
	$data = array(
		'nama'=>$profil->nama_lengkap,
		'jk'=>$profil->jk,
		'agama'=>$profil->agama,
 		'alamat'=>$profil->alamat,
		'tempat_tanggal_lahir'=>$profil->tempat_tanggal_lahir,
	);
}else{
	$data = array(
		'nama'=>"",
		'jk'=>"",
		'agama'=>"",
 		'alamat'=>"",
		'tempat_tanggal_lahir'=>"",
	);	
	
	
}
 ?>
 <form action="<?=site_url('caleg/update_profil');?>" method='post'  id='form_profil' class="form">
	<div class="field-group"> 
		<label for="">Nama Lengkap</label>
		<input type="text" name='nama' value="<?=$data['nama'];?>" class="input-xlarge" />
		<input type="hidden" name='id_caleg' value="<?=$row->id_caleg;?>" class="form-control"/>
	</div>	
	<div class="field-group"> 
		<label for="">Tempat/ Tanggal Lahir</label>
		<input type="text" name='ttl' value="<?=$data['tempat_tanggal_lahir'];?>" class="input-xlarge" />
	</div>	
	<div class="controls"> 
		<label for="">Jenis Kelamin & Agama</label>
	 
		<select name="jk" class="span2">
			<option value="L" <?=($data['jk']=="L")?"selected":null?> >Laki-laki</option>
			<option value="P" <?=($data['jk']=="P")?"selected":null?>>Perempuan</option>
		</select>	
 		<select name="agama" class="span2">
			<option value="1" <?=($data['agama']=="1")?"selected":null?>>Islam</option>
			<option value="2" <?=($data['agama']=="2")?"selected":null?>>Kristen</option>
			<option value="3" <?=($data['agama']=="3")?"selected":null?>>Katolik</option>
			<option value="4" <?=($data['agama']=="4")?"selected":null?>>Hindu</option>
			<option value="5" <?=($data['agama']=="5")?"selected":null?>>Budha</option>
			<option value="6" <?=($data['agama']=="6")?"selected":null?>>Konghocu</option>
		</select>		
	</div>		
 	
	<div class="field-group"> 
		<label for="">Alamat Lengkap</label>
		<input type="text" name='alamat' value="<?=$data['alamat'];?>" class="input-xxlarge" />
	</div>	
	  <div id='sukses-1' class="alert alert-success hide">
		Berhasil menyimpan..
	  </div> 
	  <div id='error-1'class="alert alert-danger hide">
		Gagal menyimpan..
	  </div>
	<div class="form-actions">
	
	  <button type="button" class="btn" onClick="self.history.back()">Cancel</button>
	  <button type="submit" class="btn btn-primary">Save changes</button>
	</div>
 </form>
 
 <script type="text/javascript"> 
	
	$(function(){
		$("#form_profil").submit(function(){
			var id = $("#URI").val();
			
			$("#sukses-1").hide();
			$("#error-1").hide();
			
			$.ajax({
					url:id+"caleg/update_profil/",
					data:$(this).serialize(),
					type:"POST",
					dataType:"json",
					success:function(msg){
						if(msg.code==1)
						$("#sukses-1").show();
						else
						$("#error-1").show();
						
					}
				});
			return false;
		});
		
	});
	
 
 
 </script>