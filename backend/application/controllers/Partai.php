<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Partai extends CI_Controller {
 
 	public function __construct()
	{
		parent::__construct();
 
		 
	}
	public function index(){
		
		$data['isi'] = 'partai/index';
		$data['data'] = $this->db->get('tb_partai')->result();
		$this->load->view('master',$data);
		
	}		
	public function tambah(){
		
		$data['isi'] = 'partai/tambah';
		
		$this->load->view('master',$data);
		
	}	
	public function edit( $id ){
		 
		$data['isi'] = 'partai/edit';
		$data['row'] = $this->db->where('id_partai',$id)->get('tb_partai')->row();
		
		$this->load->view('master',$data);
		
	}	
	
	public function insert(){
		
		$i = $this->input;
		$f = $_FILES['file'];
		$allowed = array("image/jpeg", "image/gif", "image/png");
		$file_type = $f['type']; //returns the mimetype

		
		if(!in_array($file_type, $allowed)) {
		  
		  $error_message = 'Only jpg, gif, and pdf files are allowed.';
		  $error = 'yes';
		  echo $error_message;
		  
		}else {
			
			move_uploaded_file($f['tmp_name'],'public/img/'.$f['name']);
			
			$data = [
				'nama_partai'=>$i->post('nama'),
				'gambar'=>$f['name'],
			];
			$this->db->insert('tb_partai',$data);
			header('location:'.site_url('partai'));
		
		}
	}
	 	
	public function update(){
		
		$i = $this->input;
		$f = $_FILES['file'];
		$allowed = array("image/jpeg", "image/gif", "image/png");
		$file_type = $f['type']; //returns the mimetype
		$id = $i->post('id');
		
		if($f['name']!=null){
			if(!in_array($file_type, $allowed)) {
			  
			  $error_message = 'Only jpg, gif, and pdf files are allowed.';
			  $error = 'yes';
			  echo $error_message;
			  
			}else {
				
				move_uploaded_file($f['tmp_name'],'public/img/'.$f['name']);
				
				$data = [
					'nama_partai'=>$i->post('nama'),
					'gambar'=>$f['name'],
				];
				$this->db->where('id_partai',$id)->update('tb_partai',$data);	
				header('location:'.site_url('partai'));
			
			}
		}else{ 
		
				$data = [
					'nama_partai'=>$i->post('nama'),
				];
				$this->db->where('id_partai',$id)->update('tb_partai',$data);	
			
			header('location:'.site_url('partai'));
		
		}
	}
	public function hapus( $id ){
		$this->db->where('id_partai',$id)->delete('tb_partai');
		header('location:'.site_url('partai'));

	}	 
	 
	 
	
}
