import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import axios from 'axios';

import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'
import Profile from '../profile/Profile.js';
import  BackButton from '../back.js';

 

export default class VootingDetailProfil extends Component {

	state = {
	  sukses:{
		  status:0,
		  msg:"",
		  img:0,
	  },
	  loading:true,
	  klik:false,
	  error:{
		  status:0,
		  msg:"",
	  },
	  status:0,
	  data:[],
	  vooted_this:0,
	  profil:[],
	  riwayat:{
		pendidikan:[],
		diklat:[],
		organ:[],
		pekerjaan:[],
	  },	  
	}
	
	vooted = ( ) =>{

	  let data = this.state.data;
	  this.setState({klik:true});
	
	  let user = {
		  id:window.localStorage.getItem("id"),
		  email:window.localStorage.getItem("email"),
		  name:window.localStorage.getItem("name"),
		  img:window.localStorage.getItem("img"),
		  token:window.localStorage.getItem("token"),
	  };
	  var self = this;
	 if( user.id == null ){
		 confirmAlert({
			 title:"Alert",
			 message:"Anda Harus Login terlebih Dahulu",
			 buttons:[{
				label:"exit",
				onClick:()=> {
					console.log('ok')
				}
			 }]
		 });
	 } else {
		 axios.post('https://tes.bulawan.news/apis/voot', {
			id_user:user.id,
			email:user.email,
			name:user.name,
			img:user.img,
			id_partai:data.id_partai,
			id_caleg:data.id_caleg,
			id_jenis:data.id_jenis,
			token:user.token,
		  })
		  .then(function (response) {
			if(response.data.kode==0){
						
						
						confirmAlert({
						  title: 'Confirm to submit',
						  message: 'anda sudah memvooting pasangan yang lain, apakah anda ingin berpindah vooting?',
						  buttons: [
							{
							  label: 'Yes',
							  onClick: () => {
									axios.post('https://tes.bulawan.news/apis/vootbypass',{
										id_user:user.id,
										email:user.email,
										name:user.name,
										token:user.token,
										img:user.img,
										id_partai:data.id_partai,
										id_caleg:data.id_caleg,
										id_jenis:data.id_jenis,
										ids:response.data.ids,
									}).then(function(response){
												self.setState({
													error:{
														status:0,
														msg:"",
													},
													sukses:{
														status:1,
														msg:response.data.msg,
													},
													vooted_this:true,
													klik:false,
													total:parseInt(self.state.total)+1,
												});
									}).catch(function(error){
												self.setState({
													error:{
													status:1,
													msg:response.data.msg,
												},
												klik:false,
												sukses:{
													status:0,
													msg:error,
												}
											});
									});
								  
							  }
							},
							{
							  label: 'No',
							  onClick: () =>{
								   self.setState({klik:false});
							  }
							}
						  ]
						})
						

					 
						
						
						
			}
			else 
					self.setState({
						error:{
							status:0,
							msg:"",
						},
						klik:false,
						sukses:{
							status:1,
							msg:response.data.msg,
						},
						vooted_this:true,
						total:parseInt(self.state.total)+1,
					})
					
					alert(this.state.sukses);
		  })
		  .catch(function (error) {
			console.log(error);
		  });
	  }
	}
	componentDidMount(){
		
		const idu = window.localStorage.getItem("id");
		let p="";
		if( idu )
			p = this.props.match.params.id+"/"+idu;
		else 
			p = this.props.match.params.id;
			
		fetch("https://tes.bulawan.news/apis/caleg_by_id/"+p)
		.then(res=>res.json())
		.then((result)=>{
			
			this.setState({
				loading:false,
				data:result.data,
				profil:result.profil,
				riwayat:{
					pendidikan:result.riwayat.pendidikan,
					diklat:result.riwayat.diklat,
					organ:result.riwayat.organ,
					pekerjaan:result.riwayat.pekerjaan,
				},
				vooted_this:result.vooted_this,
				total:result.total_voot,
			});
			
		});
		
	}
	nofoto=(data)=>{
		if(data=="")
		return(
		<img alt='gambar caleg' src={"/img/caleg.jpg"} className='img-responsive img-circle' style={{width:'70%'}} />
		);
		else 
		return(
		
		<img alt='gambar caleg' src={"/img/"+data} className='img-responsive ' style={{width:'100%'}}/>

		);
	}
	getCaleg=()=>{
		const { data,profil,riwayat,total } = this.state;
		let  p = this.props.match.params.id;
		  const sukses = (this.state.sukses.status) ?
			(
			  <div className="alert alert-success">
			  {this.state.sukses.msg}
			  </div>
			)
		  :null ;		

		  const error = (this.state.error.status) ?
			(
			  <div className="alert alert-danger">
			  {this.state.error.msg}
			  </div>
			)
		  :null ;
		  
		  if(this.state.loading)
			return(
					<div> 
						<center> 
						<img src="/img/loading.gif" width='100' />
						<strong>Loading...</strong>
						</center>
					</div>
				)
		  else 
		  
		   return ( <div>
			
					
					<div className="col col-md-12 profil">
						
						<h3 className='left'><img src={"/img/"+data.gambar} height="50" alt={data.nama_partai}   /> {data.id_partai}. {data.nama_partai}</h3>
						<hr />
					  <div className="col col-md-3">
						{ this.nofoto(data.foto) }
						
						<h4> <i className="far fa-thumbs-up"></i>  {total} Voting </h4>
						 
					 </div>
					 <div className="col col-md-9">
					   <ul className='list-group'>
						 <li className="list-group-item">
							<h5>NOMOR : {data.no_urut}   </h5>
						 </li> 
						<li className="list-group-item">
							 <h5> Calon Anggota {data.jenis}</h5>
						   </li>					 
						 <li className="list-group-item">
							<h4>{data.nama_caleg}   </h4>
						 </li>
						 <li className="list-group-item">
							<Profile data={data} profil={profil} riwayat={riwayat} total={total} />
						 </li>
						 

							
					   </ul>
						{sukses}
						{error}
						<div className="col col-md-3 row">
						 <button onClick={this.vooted} className="btn btn-success full " disabled={this.state.vooted_this||this.state.klik}><i className='far fa-thumbs-up'></i> { this.state.vooted_this?"voted":"Voting"}</button>
					   </div>
					 </div>

					</div>
				</div>
			)
		
		
		
	}
	render(){

		  
	 
		  return(
		  
			<div className='container'>
 			  <div className="row">
				<div className="col col-md-12">
					<BackButton />
				</div>
				
				 {this.getCaleg()}
				 
				</div>
			</div>


		  )
		}

}
