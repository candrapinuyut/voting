<blockquote><h4>Riwayat Diklat</h4></blockquote>
<hr />

<?php 

$data_field=[
	'nama_diklat','tempat','tahun'
];
$data_label=[
	'Nama Diklat','Tempat Pelaksanaan','Tahun Penyelenggaraan'
];

?>
<form action="<?=site_url('caleg/update_riwayat_diklat');?>" method='post'  id='form-3' class="form">


	<input type="hidden" name='id_caleg' id='id_caleg' value="<?=$row->id_caleg;?>" class="form-control"/>
 
	<?php 
	$i=0;
	foreach($data_label as $f) {
	?>
	<div class="field"> 
		<label for=""><?=ucwords(str_replace("_"," ",$f));?></label>
		<input type="text" name='<?=$data_field[$i];?>' class="input-xlarge" />
	</div>	
	<?php $i++; } ?>

 
	  <div id='sukses-3' class="alert alert-success hide">
		Berhasil menyimpan..
	  </div> 
	  <div id='error-3'class="alert alert-danger hide">
		Gagal menyimpan..
	  </div>
 
	  <button type="submit" class="btn btn-primary">Save changes</button>
</form>
<table class='table table-bordered table-striped'>
	<tr>
		<th width=10>NO</th>
		<th>NAMA DIKLAT</th>
		<th>TEMPAT PEL.</th>
		<th>TAHUN</th>
		<th></th>
	</tr>
	<tbody id='result-3'>
	</tbody>
</table>
 
 <script type="text/javascript"> 
	
	var id = $("#URI").val();
	function hapus_riwayat_diklat(idcaleg)
	{
		if(window.confirm("hapus?")){
		$.ajax({
			url:id+"caleg/hapus_riwayat_diklat/"+idcaleg,
			type:"POST",
			dataType:"HTML",
			success:function(msg){
				$("#"+idcaleg).remove();
			}
		});
		}
		return false;
	}
	function result3(){
		var idcaleg = $("#id_caleg").val();
		$("#result-3").html('');
		$.ajax({
			url:id+"caleg/caleg_riwayat_diklat_by_id_caleg/"+idcaleg,
			type:"POST",
			dataType:"HTML",
			success:function(msg){
				
				$("#result-3").html(msg);
				
			}
		});
		
	}
	$(function(){
		 result3();
		$("#form-3").submit(function(){
			
			$("#sukses-3").hide();
			$("#error-3").hide();
			
			$.ajax({
					url:id+"caleg/update_riwayat_diklat",
					data:$(this).serialize(),
					type:"POST",
					dataType:"json",
					success:function(msg){
						if(msg.code==1){
							setTimeout(function(){
								$("#sukses-3").hide();
							},2000);
							$("#sukses-3").show();
							$("input[type='text']").val('');
							 result3();
						}
						else
						$("#error-3").show();
						
					}
				});
			return false;
		});
		
	});
	
 
 
 </script>