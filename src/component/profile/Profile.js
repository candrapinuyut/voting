import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import axios from 'axios';

  



export default class Profile extends Component {
	
	state={
		data:[],
		profil:[],
		riwayat:[],
	}
	getCalegProfil=( profil )=>{
		let style={
			width:"100%",
			padding:'10'
		}
		if(profil)
		return(
				<div className='row'>
				<h4>Profile lengkap</h4>
				<table className='table table-striped' style={style}>
					<tr>
						<td width='150'>Nama Lengkap</td>
						<td>:</td>
						<td>{profil.nama_lengkap}</td>
					</tr>
					<tr>
						<td>Alamat Lengkap</td>
						<td>:</td>
						<td>{profil.alamat}</td>
					</tr>
					<tr>
						<td>Jenis Kelamin</td>
						<td>:</td>
						<td>{profil.jk}</td>
					</tr>
					<tr>
						<td>Agama</td>
						<td>:</td>
						<td>{profil.agama}</td>
					</tr>
					<tr> 
						<td>Status Kawin</td>
						<td>:</td>
						<td>{profil.status_kawin}</td>
					</tr>
				</table>
				</div>
		)
		else return("");
		
	}
	getCalegRiwayatPendidikan( riwayat ){
		if( riwayat.pendidikan.length>0 )
		return( 
			
			<div class='row'>
			
				<h4>Riwayat Pendidikan</h4>
				<ul>
				{
				  riwayat.pendidikan.map(f=>(
					<li>{f.prodi}, {f.tempat}, {f.tahun_masuk} - {f.tahun_keluar}</li>
				  ))
				}
				</ul>
			 
			
			</div>
		
		
		);
		else return""
		
	}	
	getCalegRiwayatDiklat( riwayat ){
		if( riwayat.diklat.length>0 )
		return( 
			
			<div class='row'>
			
				<h4>Riwayat Diklat</h4>
				<ul>
				{
				  riwayat.diklat.map(f=>(
					<li> {f.nama_diklat}, {f.tempat}, {f.tahun}</li>
				  ))
				}
				</ul>
			 
			
			</div>
		
		
		);
		else return""
		
	}	
	getCalegRiwayatPekerjaan( riwayat ){
		if( riwayat.pekerjaan.length>0 )
		return( 
			
			<div class='row'>
			
				<h4>Riwayat Pekerjaan</h4>
				<ul>
				{
				  riwayat.pekerjaan.map(f=>(
					<li> {f.jabatan}, {f.nama_perusahaan}, {f.masa_jabatan_awal} - {f.masa_jabatan_akhir}</li>
				  ))
				}
				</ul>
			 
			
			</div>
		
		
		);
		else return""
		
	}	
	getCalegRiwayatOrgan( riwayat ){
		if( riwayat.organ.length>0 )
		return( 
			
			<div class='row'>
			
				<h4>Riwayat Organisasi</h4>
				<ul>
				{
				  riwayat.organ.map(f=>(
					<li> {f.jabatan}, {f.nama_organ}, {f.tahunjabatan_awal} - {f.tahun_jabatan_akhir}</li>
				  ))
				}
				</ul>
			 
			
			</div>
		
		
		);
		else return""
		
	}
	render(){
		
		let {data,profil,riwayat} = this.props;
		console.log(profil);
 		return(
			<div className='container'>
			{this.getCalegProfil(profil)}
			{this.getCalegRiwayatPendidikan(riwayat)}
			{this.getCalegRiwayatPekerjaan(riwayat)}
			{this.getCalegRiwayatDiklat(riwayat)}
			{this.getCalegRiwayatOrgan(riwayat)}
			</div>
		)
	}
}