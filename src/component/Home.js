import React, { Component } from 'react';
import HeaderTop from './layout/HeaderTop.js';
import {Link} from 'react-router-dom';
import Slider from './slider.js';

class Home extends Component {

    render(){
      return(

        <div  class='home container'>
           <HeaderTop />
			
			<Slider  />
			
            <div className='col col-md-6 kategori'>

               
               <div className="item bg-ri">
                   <Link to="/vooting/detail/1"> <h3>DPR RI</h3> (DAPIL SULUT)</Link>
               </div>

            </div>
            <div className='col col-md-6 kategori'>
              <div className="item bg-dprd">
                <Link to="vooting/detail/2"><h3>DPRD PROVINSI SULUT </h3>(DAPIL BMR )</Link>
              </div>
            </div>
            <div className='col col-md-4 kategori dapil-kota'>
              <div className="item bg-dapil1">
                  <Link to="vooting/detail/3"><h4>DPRD KOTA KOTAMOBAGU</h4>( DAPIL BARAT )</Link>
              </div> 
            </div>
			<div className='col col-md-4 kategori dapil-kota'>
			  <div className="item bg-dapil2">
                  <Link to="vooting/detail/4"><h4>DPRD KOTA KOTAMOBAGU</h4>  ( DAPIL UTARA-SELATAN )</Link>
              </div> 
            </div> 
			<div className='col col-md-4 kategori dapil-kota'>
			  <div className=" item bg-dapil3">
                  <Link to="vooting/detail/5"><h4>DPRD KOTA KOTAMOBAGU </h4>( DAPIL TIMUR )</Link>
              </div>
            </div>
			<div  className='col col-md-12' style={{marginTop:'40px'}}>
				 
				<center> 
					
						<img src="img/sponsor/nyiurtech.png" alt="" width="150" style={{marginRight:'40px'}} />
						<img src="img/sponsor/kotamobagukota.png" alt="" width="100" style={{marginRight:'40px'}} />
						<img src="img/sponsor/bmr24.png" alt="" width="100" style={{marginRight:'40px'}} />
						<img src="img/sponsor/satukota.png" alt="" width="90" />
				</center>
			</div>
        </div>

      )
    }

}

export default Home;
