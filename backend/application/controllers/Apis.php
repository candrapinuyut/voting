<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Apis extends CI_Controller {
 
 	public function __construct()
	{
		parent::__construct();
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods:*');
		header('Access-Control-Allow-Headers: *');
		 
		 
	}
	public function caleg(){
		
		$f = $this->db->query(" select * from tb_caleg a 
								LEFT JOIN tb_partai b on a.id_partai=b.id_partai
								LEFT JOIN tb_jenis c on a.jenis=c.id_jenis
							 ")->result();
		
		echo json_encode($f);
		
	}	
	public function caleg_by_dapil( $id ){
		
		
		
		$f = $this->db->query(" select * from tb_partai 
								
								
							 ")->result();
		$d=[];
		$i=0;
		foreach( $f as $r ){
				   
				    $rows=$this->db->query(" select a.*,c.* from tb_caleg a 
								LEFT JOIN tb_partai b on a.id_partai=b.id_partai
								LEFT JOIN tb_jenis c on a.jenis=c.id_jenis 
								WHERE a.id_partai=".$r->id_partai." AND c.id_jenis=".$id." order by a.no_urut");
					if( $rows->num_rows() > 0 ){
						$d['data'][$i]['partai']=$r;
						$d['data'][$i]['caleg']=$rows->result();
						$i++;
					}
				 
			
		}
		$jen =$this->db->where('id_jenis',$id)->get('tb_jenis')->row();
		
		$d['dapil']=$jen->jenis;
		echo json_encode($d);
		
	}	
	public function caleg_by_id( $id ,$idu=null){
		
		$f=[];
		$f['vooted_this']=false;
		if( $idu ){
			$q = $this->db->where('id_user',$idu)->where('id_caleg',$id)->get('tb_pemilih')->num_rows();
			$f['vooted_this']=($q>0)?true:false;
		}
		
		$f['data'] = $this->db->query(" SELECT  * FROM tb_caleg a 
								LEFT JOIN tb_partai b on a.id_partai=b.id_partai
								LEFT JOIN tb_jenis c on a.jenis=c.id_jenis 
								WHERE a.id_caleg=".$id)->row();
		$f['profil'] = $this->db->query("SELECT * FROM `tb_caleg_profil`  where id_caleg=$id")->row();
		$f['riwayat']['pendidikan'] = $this->db->query("SELECT * FROM `tb_caleg_riawayat_pend`  where id_caleg=$id")->result();
		$f['riwayat']['diklat'] = $this->db->query("SELECT * FROM `tb_caleg_riwayat_diklat`  where id_caleg=$id")->result();
		$f['riwayat']['organ'] = $this->db->query("SELECT * FROM `tb_caleg_riwayat_organ`  where id_caleg=$id")->result();
		$f['riwayat']['pekerjaan'] = $this->db->query("SELECT * FROM `tb_caleg_riwayat_pekerjaan`  where id_caleg=$id")->result();
		$f['total_voot']=$this->count_voted_by_caleg( $id );
		
		echo json_encode($f);
		
	}	
	function is_obj( &$object, $check=null, $strict=true )
	{
		if( $check == null && is_object($object) )
		{
			return true;
		}
		if( is_object($object) )
		{
			$object_name = get_class($object);
			if( $strict === true )
			{
				if( $object_name == $check )
				{
					return true;
				}
			}
			else
			{
				if( strtolower($object_name) == strtolower($check) )
				{
					return true;
				}
			}
		}
	}	
	public function cekValidUser( $token ){
		error_reporting(0);
		$token=urlencode($token);
		$curl_handle=curl_init();
		curl_setopt($curl_handle, CURLOPT_URL,"https://graph.facebook.com/me?access_token=$token");
		curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
		curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Your application name');
		$query = curl_exec($curl_handle);
		curl_close($curl_handle);
		$data = json_decode($query);

		if(is_object($data))
			if($data->name)
				return true;
			else
				return false;
		else 
			return false;
		
		
	}
	public function voot(){
		$data = json_decode(file_get_contents('php://input'), true);
		$d=[];
		$q = array(
			'id_user'=>$data['id_user'],
			'img'=>$data['img'],
			'nama'=>$data['name'],
			'email'=>$data['email'],
			'id_caleg'=>$data['id_caleg'],
			'jenis'=>$data['id_jenis'],
			'tgl_pemilih'=>date('Y-m-d H:i:s'),
		);
		$cek = $this->db->query("select * from tb_pemilih WHERE id_user='".$data['id_user']."' and jenis='".$data['id_jenis']."' ");
		if($cek->num_rows()>0)
		{
			$f=$cek->row();
			$d['ids']=base64_encode($f->id_pemilih);
			$d['kode']=0;
			$d['msg']="anda sudah melakukan vooting";
		}else{
			if($this->cekValidUser($data['token'])){
				$q = $this->db->insert('tb_pemilih',$q);
				
				$d['kode']=1;
				$d['msg']='anda berhasil melakukan vooting ';
			}else{
				$d['kode']=2;
				$d['token']=$data['token'];
				$d['msg']='token salah';
			}
		}
		echo json_encode($d);
	}	
	
	public function count_voted_by_caleg( $id ){
		$f = $this->db->query(" select count(*) as total from tb_pemilih WHERE id_caleg=$id
							 ")->row();
		
		return $f->total;
	}
	
	
	public function vootbypass(){
		$data = json_decode(file_get_contents('php://input'), true);
		$d=[];
		$q = array(
			'id_user'=>$data['id_user'],
			'img'=>$data['img'],
			'nama'=>$data['name'],
			'email'=>$data['email'],
			'id_caleg'=>$data['id_caleg'],
			'jenis'=>$data['id_jenis'],
			'tgl_pemilih'=>date('Y-m-d H:i:s'),
		);
		$cek = $this->db->query("select * from tb_pemilih WHERE id_user='".$data['id_user']."' and jenis='".$data['id_jenis']."' ");
		if($cek->num_rows()>0){
			if($this->cekValidUser($data['token'])){
				$d['kode']=2;
				$q=$this->db->where('id_pemilih',base64_decode($data['ids']))->update('tb_pemilih',$q);
				$d['msg']='anda berhasil melakukan vooting ';
			}else {
				$d['kode']=3;
				$d['token']=$data['token'];
				$d['msg']='Masa sessi habis, silahkan logout dan login kembali';
			}
 		}else{
			if($this->cekValidUser($data['token'])){
				$q = $this->db->insert('tb_pemilih',$q);
				$d['kode']=1;
				$d['msg']='anda berhasil melakukan vooting ';
			}else{
				$d['kode']=3;
				$d['token']=$data['token'];
				$d['msg']='Masa sessi habis, silahkan logout dan login kembali';
			}
 		}
		echo json_encode($d);
	}
	 
	function getChart($id){
	
			/* {
				{
					jens:1,
					label:[],
					background:[],
					value:[],
				}
			} */
		$f = $this->db->query(" SELECT 

					tb_caleg.no_urut,tb_partai.nama_partai,tb_partai.rgb,tb_caleg.nama_caleg,tb_pemilih.id_caleg,count(tb_pemilih.id_caleg) as total,tb_pemilih.jenis  

					FROM `tb_pemilih` 
					left join tb_caleg on tb_pemilih.id_caleg=tb_caleg.id_caleg
					left join tb_partai on tb_caleg.id_partai=tb_partai.id_partai
					where tb_pemilih.jenis=$id group by tb_pemilih.id_caleg order by total DESC LIMIT 5
		")->result();
		$a=[];
		$i=0;
		foreach($f as $r){
			
			$a['jenis']=$r->jenis;
			$a['label'][$i]=$r->no_urut.') '.$r->nama_caleg.'('.$r->nama_partai.')';
			$a['value'][$i]=$r->total;
			$a['background'][$i]=$r->rgb;
			
			$i++;
		}
		
		return ($a);
		
	}
	
	function getChartByIdDapil( $id ){
		$f=array(
				'jenis'=>$id,
				'd'=>$this->getChart($id)
		);
		echo json_encode($f);
	}
	
	function getCharts(){
		$f=[];
		$x=1;
		for($i=0;$i<5;$i++){
			$f[$i]=array(
				'jenis'=>$x,
				'd'=>$this->getChart($x)
			);
			$x++;
		}
		
		echo json_encode($f);
		
		
	}
	function simulasi(){
	
		
		$let = $this->db->get('tb_caleg')->result();
		foreach($let as $d){
			for($i=0;$i<rand(00,99);$i++){
				$this->db->insert('tb_pemilih',array('id_caleg'=>$d->id_caleg,'jenis'=>$d->jenis));
			}
		}
		
		
	}
}
