import React, { Component } from 'react';

export default class BackButton extends Component {
  static contextTypes = {
    router: () => true, // replace with PropTypes.object if you use them
  }

  render() {
    return (
      <button
        className="btn btn-default pull-right"
        onClick={this.context.router.history.goBack}>
          Back
      </button>
    )
  }
}