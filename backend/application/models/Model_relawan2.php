<?php 


class Model_relawan extends CI_Model{
	
	private $table,$id;
	
	function __construct(){
		parent::__construct();
		$this->table = 'relawan';
		$this->id    = 'id';
	}
	
	
	public function all( $cari ){
		
		$a=[];
		if( $cari['kecamatan']!=0){
		   $a[]=" d.id_kec='$cari[kecamatan]' ";
		}
		if( $cari['kelurahan']!=0){
		   $a[]=" c.id_kel='$cari[kelurahan]' ";
		}
		if( $cari['kabupaten']!=0){
		   $a[]=" e.id_kab='$cari[kelurahan]' ";
		}
		if( $cari['nama']!=null){
		   $a[]=" a.nama like'%$cari[nama]%' ";
		}
		$w=null;
		if( count($a) > 0){
			$aa = implode(" AND ",$a);
			$w= " where ".$aa;
			
		}
		
 		$d = $this->db->query("
			select * from relawan a 
			left join kel c on a.id_kel=c.id_kel 
			left join kec d on c.id_kec=d.id_kec
			left join kab e on e.id_kab=d.id_kab
			$w
		")->result();
		
		return $d;
	
	}
	public function singel( $id ){
		
		
 		$d = $this->db->query("
			select * from relawan a 
			left join kel c on a.id_kel=c.id_kel 
			left join kec d on c.id_kec=d.id_kec
			left join kab e on e.id_kab=d.id_kab
			where a.id='$id'
		")->row();
		
		return $d;
	
	}
 
	public function insert( $data){
		
		$d = $this->db->insert($this->table,$data);
		
		return $d;
	
	}	
	public function update( $data,$id){
		
		$this->db->where($this->id,$id);
		$d = $this->db->update($this->table,$data);
		
		return $d;
	
	}	
	public function hapus( $id){
		
		$this->db->where($this->id,$id);
		$d = $this->db->delete($this->table);
		
		return $d;
	
	}
	 
	 
}