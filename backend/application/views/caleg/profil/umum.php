<blockquote><h4>Data Umum</h4></blockquote>
<hr />



 <form action="<?=site_url('caleg/update');?>" method='post' enctype='multipart/form-data' class="form">
	
	<div class="field-group"> 
		<label for="">Partai</label>
		<?php 
		$p=$this->db->get('tb_partai')->result();
		?>
		<select name="partai" id="" class="input-xlarge">
		<?php foreach($p as $d) {
			if($d->id_partai==$row->id_partai){
		?>
		<option selected value="<?=$d->id_partai;?>"><?=$d->nama_partai;?></option>
		<?php } else { ?>
		<option value="<?=$d->id_partai;?>"><?=$d->nama_partai;?></option>
		
		<?php 
			}
		}
		?>
		</select>
	</div>	
	<div class="field-group"> 
		<label for="">No.URUT</label>
		<input type="text" name='no_urut' value="<?=$row->no_urut;?>" class="input-mini" />
		<input type="hidden" name='id' value="<?=$row->id_caleg;?>" class="input-mini" />
	</div>	
	<div class="field-group"> 
		<label for="">Jenis</label>
		<?php 
		$p=$this->db->get('tb_jenis')->result();
		?>
		<select name="jenis" id="" class="input-xlarge">
		<?php foreach($p as $d) {
			if($d->id_jenis==$row->id_jenis){
		?>
		<option selected value="<?=$d->id_jenis;?>"><?=$d->jenis;?></option>
		<?php 
			}else {
		?>
		<option value="<?=$d->id_jenis;?>"><?=$d->jenis;?></option>
		<?php 
			}
			
		} ?>
		</select>		
	</div>
	<div class="field-group"> 
		<label for="">Nama Lengkap</label>
		<input type="text" name='nama'  value="<?=$row->nama_caleg;?>"  class="input-xxlarge" />
	</div>	
	
	<div class="field-group"> 
		<label for="">Foto</label>
		<img src="<?=site_url('public/img/'.$row->foto);?>" alt="" width=100/>
		<br />
		<input type="file" name='file' class="form-control" />
	</div>	
	<div class="form-actions">
	  <button type="button" class="btn" onClick="self.history.back()">Cancel</button>
	  <button type="submit" class="btn btn-primary">Save changes</button>
	</div>

</form>