import React, { Component } from 'react';

export default class Footer extends Component {

    render(){
		
	
      return(
           <div className="fluid-container">
				
		
				
			    <div className="container center">
				
						<div className="col col-md-12" style={{paddingBottom:'10px'}}>
							<hr />
							<span>Copyright 2018 &copy; <a href="http://google.com">Anak Muda Kreatif BMR.</a>  Allright Reserverd </span>
						</div>
				</div>
        </div>
      )
    }

}
