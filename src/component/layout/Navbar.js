import React, { Component } from 'react';
import {Link,NavLink} from 'react-router-dom';

export default class Navbar extends Component {
	
	
	
	
    render(){
	  let styles= {
 		  top:0,
		  right:0,
		  marginTop:"-5px"
	  }
	 
      return(
        <div>
        <nav className="navbar navbar-default">
            <div className="container">
			
             <div className="navbar-header">
              <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span className="sr-only">Toggle navigation</span>
                <span className="icon-bar"></span>
                <span className="icon-bar"></span>
                <span className="icon-bar"></span>
              </button>
              <a className="navbar-brand" href="http://google.com">
			  	 
				<div style={styles}><img src="/img/logo-2.png" width="120" alt='logo'/></div>
		 
			  </a>
            </div>

             <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <ul className="nav navbar-nav navbar-right">
                <li><Link to="/">Beranda</Link></li>
                <li><NavLink to="/sponsor">Tentang Kami</NavLink></li>
                 <li><NavLink to="/laporan">Laporan</NavLink></li>

              </ul>

            </div>
            </div>
			  	
			
            </nav>
 	 
        </div>
	
      )
    }

}
