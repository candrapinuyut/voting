
<h3>Data Caleg</h3>
<hr />

<div class="input-append pull-left">
  <input class="span5" id="cari" placeholder="masukan nama caleg..."   type="text">
  <button class="btn" type="button">Search</button>
 </div>

<a href="<?=site_url('caleg/tambah');?>" class="btn btn-primary pull-right">Tambah</a>
<table class='table table-bordered table-striped'>
	<tr>
		<th width=10>NO</th>
		<th>NO URUT</th>
		<th>DAPIL</th>
		<th>NAMA LENGKAP</th>
		<th>FOTO</th>
		<th>PARTAI</th>
		<th></th>
	</tr>
	<tbody id="result">
		<?php 

		$no=$page+1;
		foreach($data as $d){
			
			if($d->foto=="")
				$d->foto="caleg.jpg";
			else 
				$d->foto=$d->foto;
			
		?>
		<tr>
			<td><?=$no;?></td>
			<td style='text-align:center'><strong><?=$d->no_urut;?></strong></td>
			<td><?=$d->jenis;?></td>
			<td><?=$d->nama_caleg;?></td>
			<td><img src="<?=site_url('public/img/'.$d->foto);?>" width=40/></td>
			<td><img src="<?=site_url('public/img/'.$d->gambar);?>" width=40/></td>
			<td> 
				<a href="<?=site_url('caleg/edit/'.$d->id_caleg);?>">Edit</a> |
				<a href="<?=site_url('caleg/hapus/'.$d->id_caleg);?>">Hapus</a>
			</td>
		</tr>
		
		<?php $no++; } ?>
	</tbody>
</table>
 
<div id='paging' class="row">
	<div class="col">
		<center> 
		<!--Tampilkan pagination-->
		<?php echo $pagination; ?>
		</center>
	</div>
</div>