
<h3>Update Caleg</h3>
<br />
<br />
<div class="tabbable"> <!-- Only required for left/right tabs -->
  <ul class="nav nav-tabs">
    <li class="active"><a href="#tab0" data-toggle="tab">Umum</a></li>
    <li><a href="#tab1" data-toggle="tab">Profile Lengkap</a></li>
    <li><a href="#tab2" data-toggle="tab">Riwayat Pendidikan</a></li>
    <li><a href="#tab3" data-toggle="tab">Riwayat Organisasi</a></li>
    <li><a href="#tab4" data-toggle="tab">Riwayat Diklat</a></li>
    <li><a href="#tab5" data-toggle="tab">Riwayat Pekerjaan</a></li>
  </ul>
  <div class="tab-content">
  
	<?php 
	$no=0;
	$w="";
	$data=['umum','profil','r_pendidikan','r_organ','r_diklat','r_pekerjaan'];
	 
	foreach($data as $d){
	if($no==0)
		$w="active";
	else $w="";
	
	?>
    <div class="tab-pane <?=$w;?>" id="tab<?=$no;?>">
      <?php $this->load->view('caleg/profil/'.$data[$no].'.php');?>
    </div>
	<?php $no++; } ?>
  </div>
</div>
