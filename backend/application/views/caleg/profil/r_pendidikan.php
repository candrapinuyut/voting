<blockquote><h4>Riwayat Pendidikan</h4></blockquote>
<hr />

<?php 

$data_field=[
	'tempat','prodi','tahun_masuk','tahun_keluar'
];
$data_label=[
	'Tempat Mengampuh','prodi/Jurusan','tahun_masuk','tahun_keluar'
];

?>
<form action="<?=site_url('caleg/update_riwayat_pendidikan');?>" method='post'  id='form-2' class="form">


	<input type="hidden" name='id_caleg' id='id_caleg' value="<?=$row->id_caleg;?>" class="form-control"/>
 	<div class="field-group">
		<label for="">Jenjang Pendidikan</label>
		<select name="jenjang" class="form-control">
			<option value="1" >SD</option>
			<option value="2">SMP</option>
			<option value="3">SMA/MA/SMK</option>
			<option value="4">S1</option>
			<option value="5">S2</option>
			<option value="6">S3</option>
 
		</select>	
	</div>
	<?php 
	$i=0;
	foreach($data_label as $f) {
	?>
	<div class="field"> 
		<label for=""><?=ucwords(str_replace("_"," ",$f));?></label>
		<input type="text" name='<?=$data_field[$i];?>' class="input-xlarge" />
	</div>	
	<?php $i++; } ?>

 
	  <div id='sukses-2' class="alert alert-success hide">
		Berhasil menyimpan..
	  </div> 
	  <div id='error-2'class="alert alert-danger hide">
		Gagal menyimpan..
	  </div>
 
	  <button type="submit" class="btn btn-primary">Save changes</button>
</form>
<table class='table table-bordered table-striped'>
	<tr>
		<th width=10>NO</th>
		<th>TEMPAT</th>
		<th>PRODI/JURUSAN</th>
		<th>TAHUN MASUK</th>
		<th>TAHUN KELUAR</th>
		<th></th>
	</tr>
	<tbody id='result-2'>
	</tbody>
</table>
 
 <script type="text/javascript"> 
	
	var id = $("#URI").val();
	function hapus_riwayat_pendidikan(idcaleg)
	{
		if(window.confirm("hapus?")){
		$.ajax({
			url:id+"caleg/hapus_riwayat_pendidikan/"+idcaleg,
			type:"POST",
			dataType:"HTML",
			success:function(msg){
				$("#"+idcaleg).remove();
			}
		});
		}
		return false;
	}
	function result2(){
		var idcaleg = $("#id_caleg").val();
		$("#result-2").html('');
		$.ajax({
			url:id+"caleg/caleg_riwayat_pendidikan_by_id_caleg/"+idcaleg,
			type:"POST",
			dataType:"HTML",
			success:function(msg){
				
				$("#result-2").html(msg);
				
			}
		});
		
	}
	$(function(){
		 result2();
		$("#form-2").submit(function(){
			
			$("#sukses-2").hide();
			$("#error-2").hide();
			
			$.ajax({
					url:id+"caleg/update_riwayat_pendidikan",
					data:$(this).serialize(),
					type:"POST",
					dataType:"json",
					success:function(msg){
						if(msg.code==1){
							setTimeout(function(){
								$("#sukses-2").hide();
							},2000);
							$("#sukses-2").show();
							$("input[type='text']").val('');
							 result2();
						}
						else
						$("#error-2").show();
						
					}
				});
			return false;
		});
		
	});
	
 
 
 </script>