<?php 


class Model_tps extends CI_Model{
	
	private $table,$id;
	
	function __construct(){
		parent::__construct();
		$this->table = 'tps';
		$this->id    = 'id_tps';
	}
	
	
	public function all( $cari ){
		
		$a=[];
		if( $cari['kabupaten']!=0){
		   $a[]=" d.id_kab='$cari[kabupaten]' ";
		}	
		if( $cari['kecamatan']!=0){
		   $a[]=" d.id_kec='$cari[kecamatan]' ";
		}
		if( $cari['kelurahan']!=0){
		   $a[]=" c.id_kel='$cari[kelurahan]' ";
		}
		$w=null;
		if( count($a) > 0){
			$aa = implode(" AND ",$a);
			$w= " where ".$aa;
			
		}
		
 		$d = $this->db->query("
			select *,a.id_tps from 
			tps a  
			left join kel c on a.id_kel=c.id_kel 
			left join kec d on c.id_kec=d.id_kec
			left join kab e on e.id_kab=d.id_kab
			$w
			group by a.id_tps
			order by d.id_kec,c.id_kel
			
		")->result();
		
		return $d;
	
	}
	public function singel( $id ){
		
		
 		$d = $this->db->query("
			select *,a.id_tps as id_tps2 from tps a  
			left join kelurahan c on a.id_kel=c.id_kel 
			left join kecamatan d on c.id_kec=d.id_kec
			where a.id_tps='$id'
		")->row();
		
		return $d;
	
	}
 
	public function insert( $data){
		
		$d = $this->db->insert($this->table,$data);
		
		return $d;
	
	}	
	public function update( $data,$id){
		
		$this->db->where($this->id,$id);
		$d = $this->db->update($this->table,$data);
		
		return $d;
	
	}	
	public function hapus( $id){
		
		$this->db->where($this->id,$id);
		$d = $this->db->delete($this->table);
		
		return $d;
	
	}
	 
	 
}