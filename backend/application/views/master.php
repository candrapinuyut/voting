<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
	<title></title>
	<link rel="stylesheet" href="<?=site_url('public/bootstrap/css/bootstrap.min.css');?>" />
	<script type="text/javascript" src="<?=site_url('public/js/jquery.min.js');?>"></script>
	<script type="text/javascript" src="<?=site_url('public/bootstrap/js/bootstrap.min.js');?>"></script>
	
</head>
<body>
	<input type='hidden' id="URI" value="<?=site_url();?>" />
	<div class="container"> 
			<div class="navbar ">
		  <div class="navbar-inner">
			<a class="brand" href="#"><img src="<?=site_url('public/img/logo-2.png');?>" alt="" width=120 /></a>
			<ul class="nav">
			  <li><a href="#">Home</a></li>
			  <?php 
		 
			  ?>
			  
			  
			  <li class="divider-vertical <?=($this->uri->segment(1)=='partai')?"active":null;?>" ><a href="<?=site_url('partai');?>">Partai Politik</a></li>
			  <li class='<?=($this->uri->segment(1)=='caleg')?"active":null;?>' ><a href="<?=site_url('caleg');?>">Caleg</a></li>
			  <li class="divider-vertical" ><a href="#">Pemilih</a></li>
			  <li ><a href="#">Laporan</a></li>
			</ul>
		  </div>
		</div>
	
	</div>
	<div class="container">
	
		<?php $this->load->view($isi);?>
	</div>
</body>

<script type="text/javascript">
	function delay(callback, ms) {
	  var timer = 0;
	  return function() {
		var context = this, args = arguments;
		clearTimeout(timer);
		timer = setTimeout(function () {
		  callback.apply(context, args);
		}, ms || 0);
	  };
	}
	$(function(){
		
		$("#cari").keyup(delay(function (e) {
			var id = $("#URI").val();
			if(id != ""){
				$("#result").html('');
				$("#paging").hide();
				$.ajax({
					url:id+"caleg/caleg_by_name/",
					data:{
						name:$("#cari").val(),
					},
					type:"POST",
					dataType:"HTML",
					success:function(msg){
						
						$("#result").html(msg);
						
					}
				});
			}
		},500));
		
	});

</script>

</html>