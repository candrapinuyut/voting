<blockquote><h4>Riwayat Pekerjaan</h4></blockquote>
<hr />

<?php 

$data_field=[
	'nama_perusahaan','jabatan','masa_jabatan_awal','masa_jabatan_akhir'
];
$data_label=[
	'Tempat Pekerjaan','Jabatan','Tahun Jabatan Awal','Tahun Jabatan Akhir'
];

?>
<form action="<?=site_url('caleg/update_riwayat_pekerjaan');?>" method='post'  id='form-5' class="form">


	<input type="hidden" name='id_caleg' id='id_caleg' value="<?=$row->id_caleg;?>" class="form-control"/>
 
	<?php 
	$i=0;
	foreach($data_label as $f) {
	?>
	<div class="field"> 
		<label for=""><?=ucwords(str_replace("_"," ",$f));?></label>
		<input type="text" name='<?=$data_field[$i];?>' class="input-xlarge" />
	</div>	
	<?php $i++; } ?>

 
	  <div id='sukses-5' class="alert alert-success hide">
		Berhasil menyimpan..
	  </div> 
	  <div id='error-5'class="alert alert-danger hide">
		Gagal menyimpan..
	  </div>
 
	  <button type="submit" class="btn btn-primary">Save changes</button>
</form>
<table class='table table-bordered table-striped'>
	<tr>
		<th width=10>NO</th>
		<th>NAMA PERUSAHAAN</th>
		<th>JABATAN</th>
		<th>TAHUN</th>
	</tr>
	<tbody id='result-5'>
	</tbody>
</table>
 
 <script type="text/javascript"> 
	
	var id = $("#URI").val();
	function hapus_riwayat_pekerjaan(idcaleg)
	{
		if(window.confirm("hapus?")){
		$.ajax({
			url:id+"caleg/hapus_riwayat_pekerjaan/"+idcaleg,
			type:"POST",
			dataType:"HTML",
			success:function(msg){
				$("#"+idcaleg).remove();
			}
		});
		}
		return false;
	}
	function result5(){
		var idcaleg = $("#id_caleg").val();
		$("#result-5").html('');
		$.ajax({
			url:id+"caleg/caleg_riwayat_pekerjaan_by_id_caleg/"+idcaleg,
			type:"POST",
			dataType:"HTML",
			success:function(msg){
				
				$("#result-5").html(msg);
				
			}
		});
		
	}
	$(function(){
		 result5();
		$("#form-5").submit(function(){
			
			$("#sukses-5").hide();
			$("#error-5").hide();
			
			$.ajax({
					url:id+"caleg/update_riwayat_pekerjaan",
					data:$(this).serialize(),
					type:"POST",
					dataType:"json",
					success:function(msg){
						if(msg.code==1){
							setTimeout(function(){
								$("#sukses-5").hide();
							},2000);
							$("#sukses-5").show();
							$("input[type='text']").val('');
							 result5();
						}
						else
						$("#error-5").show();
						
					}
				});
			return false;
		});
		
	});
	
 
 
 </script>