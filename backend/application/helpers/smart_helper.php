<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
if (!function_exists('auto_inc')) {

    function auto_inc($model_name, $pk) {
        $CI = & get_instance();
        $id = $CI->$model_name->get_last_id();
        if (!empty($id)) {
            $idd = $id->$pk + 1;
        } else {
            $idd = '1';
        }
        return $idd;
    }

}

if (!function_exists('uid')) {

    function uid() {
        $rand =  random_string('numeric', 10);
        return ltrim($rand,0);
    }

}

if (!function_exists('send_mail_confirm')) {

    function send_mail_confirm($email, $subject, $message) {
        $CI = & get_instance();
        $config = array();
        $config['charset'] = 'utf-8';
        $config['useragent'] = 'Codeigniter';
        $config['protocol'] = "smtp";
        $config['mailtype'] = "html";
        $config['smtp_host'] = MAIL_HOST;
        $config['smtp_port'] = MAIL_PORT;
        $config['smtp_timeout'] = "400";
        $config['smtp_user'] = MAIL_USER;
        $config['smtp_pass'] = MAIL_PASS;
        $config['crlf'] = "\r\n";
        $config['newline'] = "\r\n";
        $config['wordwrap'] = TRUE;
        $CI->email->initialize($config);
        $CI->email->from($config['smtp_user']);
        $CI->email->to($email);
        $CI->email->subject($subject);
        $CI->email->message($message);
        if ($CI->email->send() == TRUE) {
            return true;
        } else {
            return false;
        }
    }

}


if (!function_exists('drop_list')) {

    function drop_list($models, $pk, $name, $label, $method = NULL, $param = NULL) {
        $CI = & get_instance();
        if ($method === NULL) {
            $get = $CI->$models->get_all();
            if (!empty($get)) {
                foreach ($get as $val) {
                    $list[''] = $label;
                    $list[$val->$pk] = $val->$name;
                }
            } else {
                $list[''] = "Tidak ada data";
            }
            return $list;
        }

        if ($param === NULL) {
            $get = $CI->$models->$method();
            if (!empty($get)) {
                foreach ($get as $val) {
                    $list[''] = $label;
                    $list[$val->$pk] = $val->$name;
                }
            } else {
                $list[''] = "Tidak ada data";
            }
            return $list;
        }

        $get = $CI->$models->$method($param);
        if (!empty($get)) {
            foreach ($get as $val) {
                $list[''] = $label;
                $list[$val->$pk] = $val->$name;
            }
        } else {
            $list[''] = "Tidak ada data";
        }
        return $list;
    }

}

if (!function_exists('query_sql')) {

    function query_sql($sql, $type = NULL) {
        $CI = & get_instance();
        if ($type === NULL) {
            return $CI->db->query($sql)->result();
        }
        return $CI->db->query($sql)->$type();
    }

}

if (!function_exists('get_menu')) {

    function get_menu($id = NULL) {
        $CI = & get_instance();
        if ($id === NULL) {
            return query_sql("seelct * from v_menu");
        }
        return query_sql("select * from v_menu where id_keamanan = '$id' order by prioritas asc");
    }

}

if (!function_exists('tgl_indo')) {

    function tgl_indo($tgl) {
        $ubah = gmdate($tgl, time() + 60 * 60 * 8);
        $pecah = explode("-", $ubah);
        $tanggal = $pecah[2];
        $bulan = bulan($pecah[1]);
        $tahun = $pecah[0];
        return $tanggal . ' ' . $bulan . ' ' . $tahun;
    }

}

if (!function_exists('bulan')) {

    function bulan($bln) {
        switch ($bln) {
            case 1:
                return "Januari";
                break;
            case 2:
                return "Februari";
                break;
            case 3:
                return "Maret";
                break;
            case 4:
                return "April";
                break;
            case 5:
                return "Mei";
                break;
            case 6:
                return "Juni";
                break;
            case 7:
                return "Juli";
                break;
            case 8:
                return "Agustus";
                break;
            case 9:
                return "September";
                break;
            case 10:
                return "Oktober";
                break;
            case 11:
                return "Nopember";
                break;
            case 12:
                return "Desember";
                break;
        }
    }

}

if (!function_exists('nama_hari')) {

    function nama_hari($tanggal) {
        $ubah = gmdate($tanggal, time() + 60 * 60 * 8);
        $pecah = explode("-", $ubah);
        $tgl = $pecah[2];
        $bln = $pecah[1];
        $thn = $pecah[0];

        $nama = date("l", mktime(0, 0, 0, $bln, $tgl, $thn));
        $nama_hari = "";
        if ($nama == "Sunday") {
            $nama_hari = "Minggu";
        } else if ($nama == "Monday") {
            $nama_hari = "Senin";
        } else if ($nama == "Tuesday") {
            $nama_hari = "Selasa";
        } else if ($nama == "Wednesday") {
            $nama_hari = "Rabu";
        } else if ($nama == "Thursday") {
            $nama_hari = "Kamis";
        } else if ($nama == "Friday") {
            $nama_hari = "Jumat";
        } else if ($nama == "Saturday") {
            $nama_hari = "Sabtu";
        }
        return $nama_hari;
    }

}
if (!function_exists('hitung_mundur')) {

    function hitung_mundur($wkt) {
        $waktu = array(365 * 24 * 60 * 60 => "tahun",
            30 * 24 * 60 * 60 => "bulan",
            7 * 24 * 60 * 60 => "minggu",
            24 * 60 * 60 => "hari",
            60 * 60 => "jam",
            60 => "menit",
            1 => "detik");
        $hitung = strtotime(gmdate("Y-m-d H:i:s", time() + 60 * 60 * 8)) - $wkt;
        $hasil = array();
        if ($hitung < 5) {
            $hasil = 'kurang dari 5 detik yang lalu';
        } else {
            $stop = 0;
            foreach ($waktu as $periode => $satuan) {
                if ($stop >= 6 || ($stop > 0 && $periode < 60))
                    break;
                $bagi = floor($hitung / $periode);
                if ($bagi > 0) {
                    $hasil[] = $bagi . ' ' . $satuan;
                    $hitung -= $bagi * $periode;
                    $stop++;
                } else if ($stop > 0)
                    $stop++;
            }
            $hasil = implode(' ', $hasil) . ' yang lalu';
        }
        return $hasil;
    }

}

if (!function_exists('selisih_waktu')) {

    function selisih_waktu($mulai, $selesai, $komen) {
        list($h, $m, $s) = explode(":", $mulai);
        $dtAwal = mktime($h, $m, $s, "1", "1", "1");
        list($h, $m, $s) = explode(":", $selesai);
        $dtAkhir = mktime($h, $m, $s, "1", "1", "1");
        $dtSelisih = $dtAkhir - $dtAwal;

        $totalmenit = $dtSelisih / 60;
        $jam = explode(".", $totalmenit / 60);
        $sisamenit = ($totalmenit / 60) - $jam[0];
        $sisamenit2 = $sisamenit * 60;
        $selisih = round($sisamenit2, 0);
        if ($jam[0] <= 0) {
            $rjam = '';
        } else {
            $rjam = $jam[0] . " jam";
        }
        if ($selisih <= 0) {
            $rme = '';
        } else {
            $rme = $selisih . " menit";
        }
        if ($rjam === '' and $rme === '') {
            $result = 'Sesuai Jadwal';
        } else {
            $result = $komen . " " . $rjam . " " . $rme;
        }
        return $result;
    }

}

if (!function_exists('curPageURL')) {

    function curPageURL() {
        $pageURL = 'http';
        if (isset($_SERVER["HTTPS"]) and $_SERVER["HTTPS"] == "on") {
            $pageURL .= "s";
        }
        $pageURL .= "://";

        if (isset($_SERVER["SERVER_PORT"]) and $_SERVER["SERVER_PORT"] != "80") {
            $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
        } else {
            $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
        }

        return $pageURL;
    }

}

if (!function_exists('sharethis')) {

    function sharethis($platform = 'facebook,twitter', $url = '') {

        $render = '';
        $platform = explode(',', $platform);

        foreach ($platform as $m) {
            if ($m == 'facebook') {
                $render .= '<a href="http://facebook.com/share.php?u=' . $url . '" target="_blank" title="Bagikan"><img src="' . base_url() . 'asset/images/facebook.png"></a>&nbsp;';
            }

            if ($m == 'twitter') {
                $render .= '<a href="http://twitter.com/home?status=' . $url . '" target="_blank" title="Bagikan"><img src="' . base_url() . 'asset/images/twitter-icon.png"></a>&nbsp;';
            }
        }

        return $render;
    }

}

if (!function_exists('set_flag')){
    function set_flag($param) {
        return url_title($param, '-', TRUE);
    }
}

if (!function_exists('get_info')){
    function get_info($param){
        if ($param == 'title'){
            return 'ULP<b>BJ</b>';
        }else if ($param == 'title_min'){
            return 'BJ';
        }else if ($param == 'favicon'){
            return base_url().'assets/intranet/dist/img/favicon.png';
        }else if ($param == 'footer'){
            return '<strong>Copyright &copy; 2016 ULP-BJ Kabupaten Gorontalo.</strong> All rights reserved';
        }
    }
}

if (!function_exists('get_parent_menu')){
    function get_parent_menu($parent){
        $menu = query_sql("select title from dyn_menu where id = '$parent'","row");
        return $menu->title;
    }
}

if (!function_exists('show_debug')){
    function v_pre($str){
        echo "<pre>";
        print_r($str);
        echo "</pre>";
    }
	
}
 
 


//add by candra
function tgl_indo3($datetimeFromMysql,$format){
	
	$time = strtotime($datetimeFromMysql);
	$myFormatForView = date($format, $time);
	return $myFormatForView;
	
}
function selisih($d1,$d2){
	
   $selisih = ((abs(strtotime ($d1) - strtotime ($d2)))/(60*60*24));
   return $selisih;
}
function tgl_indo2($tgl) {
 	$pecah = explode("-", $ubah);
	$tanggal = $pecah[2];
	$bulan = substr(bulan($pecah[1]),0,3);
	$tahun = $pecah[0];
	return $tanggal . ' ' . $bulan . ' ' . $tahun;
}
function is_not($str){
	return($str==null)?"-":$str;
}

function get_header($field) {
    $headers = apache_request_headers();
	if(isset($headers[$field]))
		return $headers[$field];
	else 
		return NULL;
}
function enkrip($str){
	$password="An1N0n9";
	$encrypted_string=openssl_encrypt($str,"AES-128-ECB",$password);
	return $encrypted_string;
}
function dekrip($str){

	$password="An1N0n9";
	$decrypted_string=openssl_decrypt($str,"AES-128-ECB",$password);

	return $decrypted_string;
}
function randomNumber($length){
    
     return time()*rand(00,99);
}