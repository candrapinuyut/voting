import React, { Component } from 'react';
import {BrowserRouter,Route,Switch} from 'react-router-dom';
import Home from './component/Home.js';
import Laporan from './component/Laporan.js'
import Sponsor from './component/Sponsor.js'
import About from './component/About.js';
import Navbar from './component/layout/Navbar.js';
import Vooting from './component/vooting/Vooting.js';
import Footer from './component/layout/Footer.js';

import VootingDetailProfil from './component/vooting/VootingDetailProfil.js';
import FacebookAuth from 'react-facebook-auth';

 


const MyFacebookButton = ({ onClick }) => (
	<div className="pull-left"> 
	  <button className="btn btn-primary" onClick={onClick}>
		Login with facebook
	  </button>
	</div>
);


const authenticate = (response) => {
  console.log(response);
  window.localStorage.setItem('auth',1);
  window.localStorage.setItem('name',response.name);
  window.localStorage.setItem('id',response.id);
  window.localStorage.setItem('email',response.email);
  window.localStorage.setItem('img',response.picture.data.url);
  window.localStorage.setItem('token',response.accessToken);
  // Api call to server so we can validate the token
   window.location.href='/';
};
class App extends Component {

  logout = ()=>{
    window.localStorage.removeItem('name');
    window.localStorage.removeItem('email');
    window.localStorage.removeItem('id');
    window.localStorage.setItem('auth',0);
    window.location.href='/';
  }
  Back = ()=>{
    window.history.back();
  }
  renderTags=()=>{
    let a = window.localStorage.getItem('auth');
    let user = {
      name:window.localStorage.getItem('name'),
      id:window.localStorage.getItem('id'),
      email:window.localStorage.getItem('email'),
    }
		if(a==1){

      return (
        <div>
          <span>
            Welcome back <a href="#">{user.name} </a> | <a href="javascript:;" onClick={this.logout}>Logout</a>
          </span>
          <br/>
		  <hr />
         </div>
      )
    }
    else return(
      <FacebookAuth
          appId="608911009526525"
          callback={authenticate}
          component={MyFacebookButton}
         />
    )


	}
  render() {



    return (

         <BrowserRouter>
           <div className="fluid-container">

             <Navbar />
             <div className="container">
               {this.renderTags()}

             </div>


              <Switch>
                <Route exact path='/' component={Home} />
                <Route  path='/about' component={About} />
                <Route  path='/laporan' component={Laporan} />
                <Route  path='/sponsor' component={Sponsor} />
                <Route  path='/vooting/detail/:id' component={Vooting} />
                <Route  path='/vooting/profil/:id' component={VootingDetailProfil} />


              </Switch>
			  <Footer />
            </div>
         </BrowserRouter>
     );
  }
}

export default App;
