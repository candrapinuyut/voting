import React, { Component } from 'react';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';
 
export default class Slider extends Component {
    render() {
        return (
			<div>
			<Carousel autoPlay={true} showThumbs={false} dynamicHeight={true}>
                <div>
                    <img src="/img/slider/banner.jpg" />

                 </div>
           
            </Carousel>
			</div>
        );
    }
}