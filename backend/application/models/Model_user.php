<?php 


class Model_user extends CI_Model{
	
	
	function __construct(){
		parent::__construct();
	}
	
	public function get_username_aval( $username ){
	  $this->db->where('username',$username);
	  return($this->db->get('tb_users')->num_rows()>0)?false:true;
	}
	public function get_user_by_token($token){
			
		$Q =  " 
				SELECT * from tb_users where token='$token'
				
				
			  ";
		$q = $this->db->query( $Q );
		
		if( $q->num_rows() > 0 ){
			return $q->row();
		}else{
			return 0;
		}
	
	}
	
	public function get_all($hal){
	
		$batas=15;
		
		if($hal==0){
			$posisi=0;
		}else{ 
			$posisi = $hal * $batas;
		}

		return $this->db->query("select * from tb_users  where level=1 limit $posisi,$batas")->result();
	
	}	
	public function get_all_cari( $data,$hal ){
		
		$d=[];
 
		if( $data['username'] != '' ){
			$d[]=" username like  '%".$data['username']."%' OR skpd like '%".$data['username']."%'";
		}
		
		$w=null;
		if(count($d)>0)
			$w=" where level=1 and ".implode(" AND ",$d);
		else 
			$w=" where level=1";
		$batas=1;
		
		if($hal==0){
			$posisi=0;
		}else{ 
			$posisi = $hal * $batas;
		}
		
		return $this->db->query("select * from tb_users ".$w." limit $posisi,$batas")->result();
	
	}	
	public function get_singel($id){
		
		$this->db->where('id_user',$id);
		return $this->db->get('tb_users')->row();
	
	}	
	public function insert ( $data ){
	
		return $this->db->insert('tb_users',$data);
	
	}	
	public function update ( $data,$id ){
		
		$this->db->where('id_user',$id);	
		return $this->db->update('tb_users',$data);
	
	}	
	public function hapus( $id ){
		
		$this->db->where('id_user',$id);	
		return $this->db->delete('tb_users');
	
	}	
}