<?php 


class Model_relawan extends CI_Model{
	
	private $table,$id;
	
	function __construct(){
		parent::__construct();
		$this->table = 'relawan';
		$this->id    = 'id_relawan';
	}
	
	
	public function all( $cari ){
		
		$a=[];
		if( $cari['kabupaten']!=0){
		   $a[]=" a.id_kab='$cari[kabupaten]' ";
		}	
		if( $cari['kecamatan']!=0){
		   $a[]=" a.id_kec='$cari[kecamatan]' ";
		}
		if( $cari['kelurahan']!=0){
		   $a[]=" a.id_kel='$cari[kelurahan]' ";
		}
		if( $cari['kelurahan']!=0){
		   $a[]=" a.jenis_relawan='$cari[jenis_relawan]' ";
		}
		$w=null;
		if( count($a) > 0){
			$aa = implode(" AND ",$a);
			$w= " where a.deleted=0 AND  ".$aa;
			
		}else $w=" where a.deleted=0 ";
		
 		$d = $this->db->query("
			select * from relawan a  
			left join jenis_relawan f   on f.id_jenis_relawan=a.jenis_relawan
			left join kel c on c.id_kel=a.id_kel 
			left join kec d on d.id_kec=a.id_kec
			left join kab e on e.id_kab=a.id_kab
			left join tps g on g.id_tps=a.id_tps
			
 			$w
		")->result();
		
		return $d;
	
	}	

	public function singel( $id ){
		
		
 		$d = $this->db->query("
			select * from relawan a  
			left join jenis_relawan f   on f.id_jenis_relawan=a.jenis_relawan
			left join kel c on c.id_kel=a.id_kel 
			left join kec d on d.id_kec=a.id_kec
			left join kab e on e.id_kab=a.id_kab
			left join tps g on g.id_tps=a.id_tps


			where a.id_relawan='$id'
		")->row();
		
		return $d;
	
	}
 
	public function insert( $data){
		
		$d = $this->db->insert($this->table,$data);
		
		return $d;
	
	}	
	public function update( $data,$id){
		
		$this->db->where($this->id,$id);
		$d = $this->db->update($this->table,$data);
		
		return $d;
	
	}	
	public function hapus( $id){
		$data=array(
		'deleted'=>1
		);
		$this->db->where($this->id,$id);
		$d = $this->db->update($this->table,$data);
		
		return $d;
	
	}
	 
	 
}