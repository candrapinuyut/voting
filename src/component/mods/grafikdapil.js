import React, { Component } from 'react';
import {Bar,HorizontalBar} from 'react-chartjs-2';
import axios from 'axios';

export default class GrafikDapil extends Component {
   state = {
	   data:[],
   }
  
  componentDidMount(){
 
		let id = this.props.id;
		fetch("https://localhost/vooting/backend/apis/getChartByIdDapil/"+id)
		.then(res=>res.json())
		.then((result)=>{
			this.setState({
				data:result.d,
			});
		});
		
   }
 
	   
   render() {
	
	let {data} = this.state;
	 
	const datax= {
        labels:data.label,
        datasets: [{
			label: " 5 Besar Voting Saat Ini ",
			backgroundColor: data.background,
			borderColor: false,
			data:data.value,
        }]
    }	
	 
    const options = {
			  annotation: {
				   annotations: [{
					   drawTime: 'afterDatasetsDraw',
					   borderColor: 'red',
					   borderDash: [2, 2],
					   borderWidth: 2,
					   mode: 'vertical',
					   type: 'line',
					   value: 10,
					   scaleID: 'x-axis-0',
				 }]
			  },
			  maintainAspectRation: false
		  };
	

    return (
		
		<div className='row'  > 
 	 
			<div className="col col-md-6 col-md-offset-3" style={{padding:'20px'}}>
				 
			<HorizontalBar data = { datax } options={options} />
			
			</div>	
			 
		
		</div>
    )
  }
}
