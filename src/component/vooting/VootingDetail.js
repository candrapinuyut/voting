import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import  BackButton from '../back.js';
import GrafikDapil from '../mods/grafikdapil.js';

export default class VootingDetail extends Component {
	  constructor(props) {
		super(props);
		this.state = {
		  error: null,
		  isLoaded: false,
		  items: [],
		  titlePage:null,
		  loading:true,
		};
	  }	 
	 componentDidMount(){
 			fetch("https://tes.bulawan.news/apis/caleg_by_dapil/"+this.props.id)
			.then(res => res.json())
			.then(
			(result) => {
			  this.setState({
				isLoaded: true,
				items: result.data,
				titlePage:result.dapil,
				loading:false,

			  });
			  
			  console.log( result );
			  
			},
			// Note: it's important to handle errors here
			// instead of a catch() block so that we don't swallow
			// exceptions from actual bugs in components.
			(error) => {
			  this.setState({
				loading:true,
				isLoaded: true,
				error
			  });
			}
		  ) 
			
	}
 
 
	getCaleg=()=>{
		const {items} = this.state;
		
		if(this.state.loading)
			return(		<div className='row'>
				
					<div className={this.state.loading?"show":"hide"} >
						<center> 
						<img src="/img/loading.gif" width='100' alt='loding' />
						<strong>Loading...</strong>
						</center>
						
					</div>
				</div>	
				)
		else 

		return(
			<div>
				 
				<GrafikDapil id={this.props.id}/>
				<div className="row"> 
					{
						items.map(f=>(
								
								<div className="col  col-md-3 partai">
									
								 
								  <div className="partai-title" >
									
									<img src={"/img/"+f.partai.gambar} height="40" alt='partai'  />
									
									 {f.partai.nama_partai}
									
								  </div>
								  <ul className='list-group'>
										
									  {

										f.caleg.map(d=>
										(	
											
											<li className={"list-group-item"}><Link to={"/vooting/profil/"+d.id_caleg}>{d.no_urut}.{d.nama_caleg} </Link></li>
										))
									  }
								   </ul>
								</div>
								
						))
					}
				</div>
			</div>
  		);
		
	}
	 
    render(){
      let  p = this.props.id;
 
	
      return(

           <div className="row container">
			<BackButton />	
			 <br />
			 <br />
			
			 <img src="https://dummyimage.com/1000x150/CCCCCC/000.png" alt="" style={{width:'100%'}}/>
            <h3 className='center'>Voting Untuk  Calon Anggota { this.state.titlePage }</h3>
			
            <hr />
				 
				
           
			   {this.getCaleg()}
 			  
			
          </div>
 

      )
    }

}
