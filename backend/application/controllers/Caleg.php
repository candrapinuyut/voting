<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Caleg extends CI_Controller {
 
 	public function __construct()
	{
		parent::__construct();
 
		 
	}
	public function index(){
		/* paging */
		
		$this->load->library('pagination');
		$config['base_url'] = site_url('caleg/index'); //site url
 		
        $config['total_rows'] = $this->db->get('tb_caleg')->num_rows() ; //total row
        $config['per_page'] = 10;  //show record per halaman
        $config["uri_segment"] = 3;  // uri parameter
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);
 
        // Membuat Style pagination untuk BootStrap v4
       $config['first_link']       = 'First';
        $config['last_link']        = 'Last';
        $config['next_link']        = 'Next';
        $config['prev_link']        = 'Prev';
        $config['full_tag_open']    = '<div class="pagination"><ul>';
        $config['full_tag_close']   = '</ul></div>';
        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']    = '</span></li>';
        $config['cur_tag_open']     = '<li class="active"><span class="page-link">';
        $config['cur_tag_close']    = '</li>';
        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close']  = '</span>Next</li>';
        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close']  = '</span></li>';
 
        $this->pagination->initialize($config);
        $data['page'] = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
 
        //panggil function get_mahasiswa_list yang ada pada mmodel mahasiswa_model. 
  
 
        $data['pagination'] = $this->pagination->create_links() ;	
		/* end paging */
		$data['isi'] = 'caleg/index';
		$data['data'] = $this->db->query("SELECT * FROM tb_caleg a LEFT JOIN tb_partai b 
			ON a.id_partai=b.id_partai
			LEFT JOIN tb_jenis c on a.jenis=c.id_jenis ORDER BY a.jenis,a.id_partai,a.no_urut
			LIMIT $data[page],$config[per_page]
		")->result();
		
		
		
		$this->load->view('master',$data);
		
	}		
	public function tambah(){
		
		$data['isi'] = 'caleg/tambah';
		
		$this->load->view('master',$data);
		
	}	
	public function edit( $id ){
		 
		$data['isi'] = 'caleg/edit';
		$data['row'] = $this->db->query('SELECT * FROM tb_caleg a LEFT JOIN tb_partai b 
			ON a.id_partai=b.id_partai
			LEFT JOIN tb_jenis c on a.jenis=c.id_jenis WHERE a.id_caleg='.$id)->row();
		$data['profil'] = $this->db->where('id_caleg',$data['row']->id_caleg)->get('tb_caleg_profil')->row();
		$this->load->view('master',$data);
		
	}	
	
	public function insert(){
		
		$i = $this->input;
		$f = $_FILES['file'];
		$allowed = array("image/jpeg", "image/gif", "image/png");
		$file_type = $f['type']; //returns the mimetype

		
		if(!in_array($file_type, $allowed)) {
		  if($f['name']==''){
			  
			$data = [
				'jenis'=>$i->post('jenis'),
				'no_urut'=>$i->post('no_urut'),
				'id_partai'=>$i->post('partai'),
				'nama_caleg'=>$i->post('nama'),
				'foto'=>'',
			];
			$this->db->insert('tb_caleg',$data);
			header('location:'.site_url('caleg'));
			  
		  }else{
		  $error_message = 'Only jpg, gif, and pdf files are allowed.';
		  $error = 'yes';
		  echo $error_message;
		  }
		}else {
			
			move_uploaded_file($f['tmp_name'],'public/img/'.$f['name']);
			
			$data = [
				'jenis'=>$i->post('jenis'),
				'no_urut'=>$i->post('no_urut'),
				'id_partai'=>$i->post('partai'),
				'nama_caleg'=>$i->post('nama'),
				'foto'=>$f['name'],
			];
			$this->db->insert('tb_caleg',$data);
			header('location:'.site_url('caleg'));
		
		}
	}
	public function caleg_by_name(){
		
		$id = $this->input->post('name');
 		$data = $this->db->query(" SELECT  * FROM tb_caleg a 
								LEFT JOIN tb_partai b on a.id_partai=b.id_partai
								LEFT JOIN tb_jenis c on a.jenis=c.id_jenis 
								WHERE a.nama_caleg LIKE '%$id%' LIMIT 20")->result();
		$no=1;
		 
		foreach($data as $d){
			if($d->foto=="")
				$d->foto="caleg.jpg";
			else 
				$d->foto=$d->foto;
			
			echo("
					<tr>
						<td>$no</td>
						<td style='text-align:center'><strong>$d->no_urut</strong></td>
						<td>$d->jenis</td>
						<td>$d->nama_caleg</td>
						<td><img src='".site_url('public/img/'.$d->foto)."' width=40/></td>
						<td><img src='".site_url('public/img/'.$d->gambar)."' width=40/></td>
						<td> 
							<a href='".site_url('caleg/edit/'.$d->id_caleg)."'>Edit</a> |
							<a href='".site_url('caleg/hapus/'.$d->id_caleg)."')>Hapus</a>
						</td>
					</tr>
			
			
			");
			$no++;
		}
	}	 	
	public function update(){
		
		$i = $this->input;
		$f = $_FILES['file'];
		$allowed = array("image/jpeg", "image/gif", "image/png");
		$file_type = $f['type']; //returns the mimetype
		$id = $i->post('id');
		
		if($f['name']!=null){
			if(!in_array($file_type, $allowed)) {
			  
			  $error_message = 'Only jpg, gif, and pdf files are allowed.';
			  $error = 'yes';
			  echo $error_message;
			  
			}else {
				
				move_uploaded_file($f['tmp_name'],'public/img/'.$f['name']);
				
				$data = [
					'jenis'=>$i->post('jenis'),
					'no_urut'=>$i->post('no_urut'),
					'id_partai'=>$i->post('partai'),
					'nama_caleg'=>$i->post('nama'),
					'foto'=>$f['name'],
				];
				$this->db->where('id_caleg',$id)->update('tb_caleg',$data);	
				header('location:'.site_url('caleg'));
			
			}
		}else{ 
		
			$data = [
				'jenis'=>$i->post('jenis'),
				'no_urut'=>$i->post('no_urut'),
				'id_partai'=>$i->post('partai'),
				'nama_caleg'=>$i->post('nama'),
 			];
				$this->db->where('id_caleg',$id)->update('tb_caleg',$data);	
			
			header('location:'.site_url('caleg'));
		
		}
	}
	public function hapus( $id ){
		$this->db->where('id_caleg',$id)->delete('tb_caleg');
		header('location:'.site_url('caleg'));

	}
	function update_profil( ){
		$i = $this->input;
		
		$data = [
			'id_caleg'=>$i->post('id_caleg'),
			'nama_lengkap'=>$i->post('nama'),
			'tempat_tanggal_lahir'=>$i->post('ttl'),
			'jk'=>$i->post('jk'),
			'agama'=>$i->post('agama'),
 			'alamat'=>$i->post('alamat'),
				
		];	
		
		$cek = $this->db->where('id_caleg',$i->post('id_caleg'))->get('tb_caleg_profil')->num_rows();
		if( $cek > 0 )
			$a=$this->db->where('id_caleg',$i->post('id_caleg'))->update('tb_caleg_profil',$data);
		else 
			$a=$this->db->insert('tb_caleg_profil',$data);
		
		$qq=[];
		if( $a )
			$qq['code']=1;
		else 
			$qq['code']=0;
		
		echo json_encode($qq);
	}	
	
	
	function update_riwayat_pendidikan( ){
		$i = $this->input;
		
		$data = [
			'id_caleg'=>$i->post('id_caleg'),
			'tempat'=>$i->post('tempat'),
			'prodi'=>$i->post('prodi'),
			'tahun_masuk'=>$i->post('tahun_masuk'),
			'tahun_keluar'=>$i->post('tahun_keluar'),
			'jenjang'=>$i->post('jenjang'),
				
		];	
		
		$a=$this->db->insert('tb_caleg_riawayat_pend',$data);
		
		$qq=[];
		if( $a )
			$qq['code']=1;
		else 
			$qq['code']=0;
		
		echo json_encode($qq);
	}
	function caleg_riwayat_pendidikan_by_id_caleg( $id ){
		
		$r= $this->db->query("SELECT * FROM `tb_caleg_riawayat_pend`  where id_caleg=$id")->result();
		$no=1;
		if(count($r)>0){
			foreach($r as $f){
				echo(
				"	
					<tr id='$f->id'> 
						<td>$no</td>
						<td>$f->tempat</td>
						<td>$f->prodi</td>
						<td>$f->tahun_masuk</td>
						<td>$f->tahun_keluar</td>
						<td> 
							<a onClick='hapus_riwayat_pendidikan($f->id)' href='javascript:;void(0)'>Hapus</a>
						</td>
					</tr>
				"
				);
				$no++;
			}
		}
	}
	function hapus_riwayat_pendidikan( $id ){
		$a=$this->db->where('id',$id)->delete('tb_caleg_riawayat_pend');
				
		$qq=[];
		if( $a )
			$qq['code']=1;
		else 
			$qq['code']=0;
		
		echo json_encode($qq);
	}
	
	
	function update_riwayat_diklat( ){
		$i = $this->input;
		
		$data = [
			'id_caleg'=>$i->post('id_caleg'),
			'nama_diklat'=>$i->post('nama_diklat'),
			'tempat'=>$i->post('tempat'),
			'tahun'=>$i->post('tahun'),
				
		];	
		
		$a=$this->db->insert('tb_caleg_riwayat_diklat',$data);
		
		$qq=[];
		if( $a )
			$qq['code']=1;
		else 
			$qq['code']=0;
		
		echo json_encode($qq);
	}
	function caleg_riwayat_diklat_by_id_caleg( $id ){
		
		$r= $this->db->query("SELECT * FROM `tb_caleg_riwayat_diklat`  where id_caleg=$id")->result();
		$no=1;
		if(count($r)>0){
			foreach($r as $f){
				echo(
				"	
					<tr id='$f->id'> 
						<td>$no</td>
						<td>$f->nama_diklat</td>
						<td>$f->tempat</td>
						<td>$f->tahun</td>
						<td> 
							<a onClick='hapus_riwayat_diklat($f->id)' href='javascript:;void(0)'>Hapus</a>
						</td>
					</tr>
				"
				);
				$no++;
			}
		}
	}
	function hapus_riwayat_diklat( $id ){
		$a=$this->db->where('id',$id)->delete('tb_caleg_riwayat_diklat');
				
		$qq=[];
		if( $a )
			$qq['code']=1;
		else 
			$qq['code']=0;
		
		echo json_encode($qq);
	}	
	
	//bagian riwayat organ
	function update_riwayat_organ( ){
		$i = $this->input;
		
		$data = [
			'id_caleg'=>$i->post('id_caleg'),
			'nama_organ'=>$i->post('nama_organ'),
			'jabatan'=>$i->post('jabatan'),
			'tahun_jabatan_awal'=>$i->post('tahun_jabatan_awal'),
			'tahun_jabatan_akhir'=>$i->post('tahun_jabatan_akhir'),
				
		];	
		
		$a=$this->db->insert('tb_caleg_riwayat_organ',$data);
		
		$qq=[];
		if( $a )
			$qq['code']=1;
		else 
			$qq['code']=0;
		
		echo json_encode($qq);
	}
	function caleg_riwayat_organ_by_id_caleg( $id ){
		
		$r= $this->db->query("SELECT * FROM `tb_caleg_riwayat_organ`  where id_caleg=$id")->result();
		$no=1;
		if(count($r)>0){
			foreach($r as $f){
				echo(
				"	
					<tr id='$f->id'> 
						<td>$no</td>
						<td>$f->nama_organ</td>
						<td>$f->tahun_jabatan_awal-$f->tahun_jabatan_akhir</td>
						<td> 
							<a onClick='hapus_riwayat_organ($f->id)' href='javascript:;void(0)'>Hapus</a>
						</td>
					</tr>
				"
				);
				$no++;
			}
		}
	}
	function hapus_riwayat_organ( $id ){
		$a=$this->db->where('id',$id)->delete('tb_caleg_riwayat_organ');
				
		$qq=[];
		if( $a )
			$qq['code']=1;
		else 
			$qq['code']=0;
		
		echo json_encode($qq);
	}	
	//bagian riwayat pekerjaan
	function update_riwayat_pekerjaan( ){
		$i = $this->input;
		
		$data = [
			'id_caleg'=>$i->post('id_caleg'),
			'nama_perusahaan'=>$i->post('nama_perusahaan'),
			'jabatan'=>$i->post('jabatan'),
			'masa_jabatan_awal'=>$i->post('masa_jabatan_awal'),
			'masa_jabatan_akhir'=>$i->post('masa_jabatan_akhir'),
				
		];	
		
		$a=$this->db->insert('tb_caleg_riwayat_pekerjaan',$data);
		
		$qq=[];
		if( $a )
			$qq['code']=1;
		else 
			$qq['code']=0;
		
		echo json_encode($qq);
	}
	function caleg_riwayat_pekerjaan_by_id_caleg( $id ){
		
		$r= $this->db->query("SELECT * FROM `tb_caleg_riwayat_pekerjaan`  where id_caleg=$id")->result();
		$no=1;
		if(count($r)>0){
			foreach($r as $f){
				echo(
				"	
					<tr id='$f->id'> 
						<td>$no</td>
						<td>$f->nama_perusahaan</td>
						<td>$f->masa_jabatan_awal-$f->masa_jabatan_akhir</td>
						<td> 
							<a onClick='hapus_riwayat_pekerjaan($f->id)' href='javascript:;void(0)'>Hapus</a>
						</td>
					</tr>
				"
				);
				$no++;
			}
		}
	}
	function hapus_riwayat_pekerjaan( $id ){
		$a=$this->db->where('id',$id)->delete('tb_caleg_riwayat_pekerjaan');
				
		$qq=[];
		if( $a )
			$qq['code']=1;
		else 
			$qq['code']=0;
		
		echo json_encode($qq);
	}
	
	
	
	 
	 
	
}
