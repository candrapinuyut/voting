import React, { Component } from 'react';
import {Bar,HorizontalBar} from 'react-chartjs-2';
import axios from 'axios';
import {Link,NavLink} from 'react-router-dom';

export default class Grafik extends Component {
   state = {
	   data1:[],
	   data2:[],
	   data3:[],
	   data4:[],
	   data5:[],
   }
  
  componentDidMount(){
 
			
		fetch("https://tes.bulawan.news/apis/getCharts")
		.then(res=>res.json())
		.then((result)=>{
			this.setState({
				data1:result[0].d,
				data2:result[1].d,
				data3:result[2].d,
				data4:result[3].d,
				data5:result[4].d,
			});
		});
		
   }
 
	   
   render() {
	
	let {data1,data2,data3,data4,data5} = this.state;
	 
	const datax1= {
        labels:data1.label,
        datasets: [{
			label: " 5 Besar Voting DPR RI DAPIL SULUT ",
			backgroundColor: data1.background,
			borderColor: false,
			data:data1.value,
        }]
    }	
	const datax2= {
        labels:data2.label,
        datasets: [{
			label: " 5 Besar Voting DPRD PROV. DAPIL BMR  ",
			backgroundColor: data2.background,
			borderColor: false,
			data:data2.value,
        }]
    }	
	const datax3= {
        labels:data3.label,
        datasets: [{
			label: " 5 Besar Voting DPRD KOTAMOBAGU DAPIL BARAT ",
			backgroundColor: data3.background,
			borderColor: false,
			data:data3.value,
        }]
    }	
	const datax4= {
        labels:data4.label,
        datasets: [{
			label: " 5 Besar Voting DPRD KOTAMOBAGU DAPIL UTARA-SELATAN ",
			backgroundColor: data4.background,
			borderColor: false,
			data:data4.value,
        }]
    }	
	const datax5= {
        labels:data5.label,
        datasets: [{
			label: " 5 Besar Voting DPRD KOTAMOBAGU DAPIL TIMUR  ",
			backgroundColor: data5.background,
			borderColor: false,
			data:data5.value,
        }]
    }
      const options = {
                          annotation: {
                               annotations: [{
                                   drawTime: 'afterDatasetsDraw',
                                   borderColor: 'red',
                                   borderDash: [2, 2],
                                   borderWidth: 2,
                                   mode: 'vertical',
                                   type: 'line',
                                   value: 10,
                                   scaleID: 'x-axis-0',
                             }]
                          },
                          maintainAspectRation: false
                      };
	

    return (
		
		<div className='row'  > 
 			<div className="col col-md-6" style={{padding:'20px'}}>
				 
			<HorizontalBar data = { datax1 } options={options}  />
			</div>
			 
			<div className="col col-md-6" style={{padding:'20px'}}>
				 
			<HorizontalBar data = { datax2 } options={options} />
			
			</div>	
					 
			 
			<div className="col col-md-6" style={{padding:'20px'}}>
				 
				<HorizontalBar data = { datax3 } options={options} />
			
			</div>
				 
			<div className="col col-md-6" style={{padding:'20px'}}>
				 
				<HorizontalBar data = { datax4 } options={options} />
			
			</div>
				 
			<div className="col col-md-6" style={{padding:'20px'}}>
				 
				<HorizontalBar data = { datax5 } options={options} />
			
			</div>
		
		</div>
    )
  }
}
