<blockquote><h4>Riwayat Organisasi</h4></blockquote>
<hr />

<?php 

$data_field=[
	'nama_organ','jabatan','tahun_jabatan_awal','tahun_jabatan_akhir'
];
$data_label=[
	'Nama Organisasi','Jabatan','Tahun Jabatan Awal','Tahun Jabatan Akhir'
];

?>
<form action="<?=site_url('caleg/update_riwayat_organ');?>" method='post'  id='form-4' class="form">


	<input type="hidden" name='id_caleg' id='id_caleg' value="<?=$row->id_caleg;?>" class="form-control"/>
 
	<?php 
	$i=0;
	foreach($data_label as $f) {
	?>
	<div class="field"> 
		<label for=""><?=ucwords(str_replace("_"," ",$f));?></label>
		<input type="text" name='<?=$data_field[$i];?>' class="input-xlarge" />
	</div>	
	<?php $i++; } ?>

 
	  <div id='sukses-4' class="alert alert-success hide">
		Berhasil menyimpan..
	  </div> 
	  <div id='error-4'class="alert alert-danger hide">
		Gagal menyimpan..
	  </div>
 
	  <button type="submit" class="btn btn-primary">Save changes</button>
</form>
<table class='table table-bordered table-striped'>
	<tr>
		<th width=10>NO</th>
		<th>NAMA ORGANISASI</th>
		<th>JABATAN</th>
		<th></th>
	</tr>
	<tbody id='result-4'>
	</tbody>
</table>
 
 <script type="text/javascript"> 
	
	var id = $("#URI").val();
	function hapus_riwayat_organ(idcaleg)
	{
		if(window.confirm("hapus?")){
		$.ajax({
			url:id+"caleg/hapus_riwayat_jabatan/"+idcaleg,
			type:"POST",
			dataType:"HTML",
			success:function(msg){
				$("#"+idcaleg).remove();
			}
		});
		}
		return false;
	}
	function result4(){
		var idcaleg = $("#id_caleg").val();
		$("#result-4").html('');
		$.ajax({
			url:id+"caleg/caleg_riwayat_organ_by_id_caleg/"+idcaleg,
			type:"POST",
			dataType:"HTML",
			success:function(msg){
				
				$("#result-4").html(msg);
				
			}
		});
		
	}
	$(function(){
		 result4();
		$("#form-4").submit(function(){
			
			$("#sukses-4").hide();
			$("#error-4").hide();
			
			$.ajax({
					url:id+"caleg/update_riwayat_organ",
					data:$(this).serialize(),
					type:"POST",
					dataType:"json",
					success:function(msg){
						if(msg.code==1){
							setTimeout(function(){
								$("#sukses-4").hide();
							},2000);
							$("#sukses-4").show();
							$("input[type='text']").val('');
							 result4();
						}
						else
						$("#error-4").show();
						
					}
				});
			return false;
		});
		
	});
	
 
 
 </script>