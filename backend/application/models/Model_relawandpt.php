<?php 


class Model_relawandpt extends CI_Model{
	
	private $table,$id;
	
	function __construct(){
		parent::__construct();
		$this->table = 'relawan_dpt';
		$this->id    = 'id_relawan_dpt';
	}
	
	
	public function all( $perpage,$from,$cari ){
		
		$a=[];
		if( $cari['kabupaten']!=0){
		   $a[]=" e.id_kab='$cari[kabupaten]' ";
		}	
		if( $cari['kecamatan']!=0){
		   $a[]=" d.id_kec='$cari[kecamatan]' ";
		}
		if( $cari['kelurahan']!=0){
		   $a[]=" c.id_kel='$cari[kelurahan]' ";
		}
		$w=null;
		if( count($a) > 0){
			$aa = implode(" AND ",$a);
			$w= " where a.status=1 and ".$aa;
			
		}else{
			$w = " where a.status=1 ";
		}
		
 		$d = $this->db->query("
			select *,f.nama as nama_dpt from relawan_dpt a left join tps b on a.id_tps=b.id_tps 
 			left join kel c on b.id_kel=c.id_kel 
			left join kec d on c.id_kec=d.id_kec
			left join kab e on d.id_kab=e.id_kab
			left join dpt f on a.id_dpt=f.id_dpt
			$w
			
			limit $from,$perpage
			
		")->result();
		
		return $d;
	
	}
	public function jumlah_data( $cari ){
		
		$a=[];
		if( $cari['kabupaten']!=0){
		   $a[]=" e.id_kab='$cari[kabupaten]' ";
		}	
		if( $cari['kecamatan']!=0){
		   $a[]=" d.id_kec='$cari[kecamatan]' ";
		}
		if( $cari['kelurahan']!=0){
		   $a[]=" c.id_kel='$cari[kelurahan]' ";
		}
		$w=null;
		if( count($a) > 0){
			$aa = implode(" AND ",$a);
			$w= " where a.status=1 and ".$aa;
			
		}else{
			$w = " where a.status=1 ";
		}
		
 		$d = $this->db->query("
			select *,f.nama as nama_dpt from relawan_dpt a left join tps b on a.id_tps=b.id_tps 
 			left join kel c on b.id_kel=c.id_kel 
			left join kec d on c.id_kec=d.id_kec
			left join kab e on d.id_kab=e.id_kab
			left join dpt f on a.id_dpt=f.id_dpt
			$w
			
 			
		")->num_rows();
		
		return $d;
	
	}	
	public function singel( $id ){
		
		
 		$d = $this->db->query("
			select * from relawan_dpt a left join tps b on a.id_tps=b.id_tps 
			left join kel c on b.id_kel=c.id_kel 
			left join kec d on c.id_kec=d.id_kec
			left join kab e on d.id_kab=e.id_kab

			where a.id_relawan_dpt='$id'
		")->row();
		
		return $d;
	
	}
 
	public function insert( $data){
		
		$d = $this->db->insert($this->table,$data);
		
		return $d;
	
	}	
	public function update( $data,$id){
		
		$this->db->where($this->id,$id);
		$d = $this->db->update($this->table,$data);
		
		return $d;
	
	}	
	public function hapus( $id){
		
		$this->db->where($this->id,$id);
		$d = $this->db->delete($this->table);
		
		return $d;
	
	}
	 
	 
}