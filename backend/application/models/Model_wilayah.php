<?php 


class Model_wilayah extends CI_Model{
	
	private $table,$id;
	
	function __construct(){
		parent::__construct();
		$this->table = 'relawan';
		$this->id    = 'id_relawan';
	}
	
	
 
	public function keluarahan(){
		return $this->db->get('kel')->result();
	}	
	public function kecamatan(){
		return $this->db->get('kec')->result();
	}
	public function kelurahanByIdKec( $id ){
		$this->db->where('id_kec',$id);
		return $this->db->get('kel')->result();
		
	}
	public function kecamatan_all( $cari ){
		$a=[];
		if( $cari['kabupaten']!=0){
		   $a[]=" b.id_kab='$cari[kabupaten]' ";
		}
		$w=null;
		if( count($a) > 0){
			$aa = implode(" AND ",$a);
			$w= " where ".$aa;
			
		}
		
		return $this->db->query('select * from kec a left join kab b on a.id_kab=b.id_kab '.$w)->result();
	}	
	public function kelurahan_all( $cari ){
		
		$a=[];
		if( $cari['kecamatan']!=0){
		   $a[]=" b.id_kec='$cari[kecamatan]' ";
		}
		if( $cari['kabupaten']!=0){
		   $a[]=" c.id_kab='$cari[kabupaten]' ";
		}
		$w=null;
		if( count($a) > 0){
			$aa = implode(" AND ",$a);
			$w= " where ".$aa;
			
		}

		
		return $this->db->query('
			select * from 
			kel a left join kec b on a.id_kec=b.id_kec 
			left join kab c on b.id_kab=c.id_kab
			'.$w.' 
			group by a.id_kel 
			
		')->result();
	}	
	public function kecamatan_edit($id){
		
		return $this->db->where('id_kec',$id)->get('kec')->row();
	}
	public function kelurahan_edit($id){
		
		return $this->db->query('
			select * from 
			kel a left join kec b on a.id_kec=b.id_kec 
			left join kab c on b.id_kab=c.id_kab
			where a.id_kel='.$id
		)->row();	
	}
	public function kecamatan_insert($data){
		return $this->db->insert('kec',$data);
	}
	public function kelurahan_insert($data){
		return $this->db->insert('kel',$data);
	}
	public function kecamatan_update($id,$data){
		$this->db->where('id_kec',$id);
		return $this->db->update('kec',$data);
	}
	public function kelurahan_update($id,$data){
		$this->db->where('id_kel',$id);
		return $this->db->update('kel',$data);
	}	
	public function kelurahan_hapus($id){
		$this->db->where('id_kel',$id);
		return $this->db->delete('kel');
	}
	public function kecamatan_hapus($id){
		$this->db->where('id_kec',$id);
		return $this->db->delete('kec');
	}
	public function tpsByIdKel( $id ){
		$this->db->where('id_kel',$id);
		return $this->db->get('tps')->result();
		
	}		
	 
	 
}